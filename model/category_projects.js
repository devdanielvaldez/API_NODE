const mongoose = require('mongoose');

const CategoryProjectsSchema = new mongoose.Schema({
    projectName: {
        type: String,
        trim: true,
        required: true
    },
    typeAdjunto: {
        type: String,
        required: true,
        trim: true
    },
    require: {
        type: String,
        required: true
    }
},
    {
        collection: 'category_projects',
        versionKey: false
    }
)

const model = mongoose.model('CategoryProjectsSchema', CategoryProjectsSchema);
module.exports = model;