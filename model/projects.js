const mongoose = require('mongoose');

const ProjectsSchema = new mongoose.Schema({
    projectName: {
        type: String,
        trim: true,
        require: true,
        unique: true
    },
},
    {
        collection: 'projects',
        timestamps: true
    }
)

const model = mongoose.model('ProjectsSchema', ProjectsSchema);
module.exports = model;