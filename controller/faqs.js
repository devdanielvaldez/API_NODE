const faq = require('../model/faqs');

exports.create = async(req, res) => {
    const {
        title,
        detail
    } = req.body;

    try {
        const response = await faq.create({
            title,
            detail
        })
        res.status(201).json('Created')
    } catch (err) {
        res.status(400).json(err)
        return
    }
}

exports.view = async(req, res) => {
    faq.find({})
        .then((data) => {
            res.status(200).json(data)
        })
}

exports.viewId = async(req, res) => {
    faq.findById(req.params)
        .then((data) => {
            res.status(200).json(data)
        })
}

exports.update = async(req, res) => {
    faq.findByIdAndUpdate({ _id: req.params._id }, req.body)
        .then(function() {
            faq.findOne({ _id: req.params._id })
                .then(function(data) {
                    res.status(200).json(data)
                })
        })
}

exports.delete = async(req, res) => {
    faq.findByIdAndRemove({ _id: req.params._id })
        .then(function(data) {
            res.status(200).json('Delete')
        })
}

exports.viewLimit = async(req, res) => {
    faq.find({}).limit(5)
        .then(data => {
            res.status(200).json(data)
        })
}