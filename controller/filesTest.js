const data = require('../model/test');

exports.createData = async (req, res) => {
    const response = await data.create({
        name: 'Daniel'
    })
    res.status(201).json('Datos guardados')
}

exports.addData = async (req, res) => {
    data.findOneAndUpdate({ _id: req.params.id }, {
        $push: {
            filesPropuesta: [
                {
                    propuesta: req.body.files
                }
            ]
        }
    })
    .then(data => {
        res.status(201).json("Success");
        console.log(data)
    })
    .catch((err) => {
        res.status(500).json(err);
    })
}