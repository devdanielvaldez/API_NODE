const Historico = require('../model/hitorico')

exports.create = async(req, res) => {
    const {
        idSolicitud,
        fase,
        accion,
        usuario
    } = req.body;

    try {
        const response = await Historico.create({
            idSolicitud,
            fase,
            accion,
            usuario
        })
        res.status(200).json('Historico registrado correctamente')

    } catch (err) {
        res.status(400).json(err)

    }
}

exports.view = async(req, res) => {
    Historico.find(req.query)
        .then(data => {
            res.status(200).json(data)

        })
        .catch((err) => {
            res.status(400).json(err)

        })
}